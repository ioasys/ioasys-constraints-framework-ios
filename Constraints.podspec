Pod::Spec.new do |s|
    s.name         = "Constraints"
    s.version      = "0.0.1"
    s.summary      = "A brief description of Constraints project."

    s.description  = <<-DESC
    An extended description of Constraints project.
    DESC

    s.homepage = "git clone https://bitbucket.org/ioasys/ioasys-constraints-ios"

    s.license = {
        :type => 'Copyright',
        :text => <<-LICENSE
            Copyright 2018
            Permission is granted to...
            LICENSE
    }

    s.author = {
        "ioasys" => "$(git config user.email)"
    }

    s.source = {
        :git => "https://bitbucket.org/ioasys/ioasys-constraints-ios.git",
        :tag => "#{s.version}"
    }

    s.vendored_frameworks = "Constraints.xcframework"
    s.swift_version = "5.4"

    s.ios.deployment_target = '11.0'
end
