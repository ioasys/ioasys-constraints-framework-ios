// swift-tools-version:5.4
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Constraints",
    platforms: [.iOS(.v11)],
    products: [
        .library(
            name: "Constraints",
            targets: ["Constraints"]
        )
    ],
    targets: [
        .binaryTarget(
            name: "Constraints",
            path: "Constraints.xcframework"
        )
    ]
)
